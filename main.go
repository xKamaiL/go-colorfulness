package main

import (
	"fmt"
	"github.com/EdlinOrg/prominentcolor"
	"github.com/labstack/echo/v4"
	"image"
	"math"
	"net/http"
)

func loadImage(fileInput string) (image.Image, error) {
	//url := "http://i.imgur.com/m1UIjW1.jpg"
	// don't worry about errors
	response, _ := http.Get(fileInput)

	defer response.Body.Close()

	img, _, err := image.Decode(response.Body)
	return img, err
}

func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		if c.QueryParam("image") == "" {
			return c.String(404, "Image not found")
		}
		img, err := loadImage(c.QueryParam("image"))
		if err != nil {
			return c.String(http.StatusBadRequest, "Failed to load image")
		}

		colours, err := prominentcolor.KmeansWithAll(7, img, 0, 300, prominentcolor.GetDefaultMasks())
		if err != nil {
			return c.String(http.StatusBadRequest, "Failed to process this image")
		}
		list := 0.2
		rgb := prominentcolor.ColorRGB{
			R: 230,
			G: 230,
			B: 230,
		}
		for _, colour := range colours {
			factor := Colorfulness(float64(colour.Color.R), float64(colour.Color.G), float64(colour.Color.B))
			if factor > 9 && factor > list {
				list = factor
				rgb = colour.Color
				fmt.Println("#" + colour.AsString())
			}
		}
		return c.JSON(200, rgb)
	})
	e.Logger.Fatal(e.Start(":5050"))
}

func Colorfulness(r float64, g float64, b float64) float64 {
	rg := r - g
	yb := (0.5 * (r + g)) - b
	rg = math.Pow(rg, 2)
	yb = math.Pow(yb, 2)
	return 0.3 * math.Sqrt(rg+yb)
}
