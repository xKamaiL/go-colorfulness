module gitlab.com/xKamaiL/go-colorfulness

go 1.15

require (
	github.com/EdlinOrg/prominentcolor v1.0.0
	github.com/labstack/echo/v4 v4.2.0
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/oliamb/cutter v0.2.2 // indirect
)
