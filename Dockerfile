FROM golang:1.15.7 as builder

WORKDIR /app

COPY . .

RUN CGO_ENABLED=0 go build -o goapp main.go

FROM alpine:latest

WORKDIR /app
COPY --from=builder /app/goapp ./goapp

EXPOSE 5050

CMD ["./goapp"]